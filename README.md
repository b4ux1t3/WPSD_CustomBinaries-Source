## This is a fork

There has been a precedent of the original maintainer of this software having an opinion on whether some servers belong on the lists presented by their UI.

It is the _opinion_ of _this_ maintainer that that sort of opinionated design is damaging to the amateur radio hobby and community.

This maintainer is not making any judgements on the motivations or opinions of the original maintainer. The goal here is purely to allow users the alternative of accessing any server in the canonical list, not to condemn or malign any other parties' for their beliefs or views.

# Description

Since I custom-compile and distribute third-party
[programs/binaries](https://repo.w0chp.net/Chipster/W0CHP-PiStar-bin) for
`W0CHP-PiStar-Dash`, I include all sources from upstream .

I also include a crude build script that builds all of the binaries.
